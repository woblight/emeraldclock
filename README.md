# EmeraldClock

Addon for World of Warcraft (retail and classic)

## Description

It display an analog clock. It can be locked/unlocked by using `/eclock lock` and `/eclock unlock` chat commands.
To open it again after closing use `/eclock show` chat command.

## Installation

If you are downloading from gitlab, extract the folder contained into `Interface\AddOns` and rename it to EmeraldClock. To check it's correctly installed be sure the `.toc` file is located at `Interface\AddOns\EmeraldClock\EmeraldClock.toc`

### Required Dependency

To work, EmeraldClock requires [EmeralFramework](https://gitlab.com/woblight/EmeraldFramework) addon to be also installed. 
