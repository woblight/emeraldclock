EmeraldClock = CreateFrame("frame")

local EFrame = EFrame
local clock

local skins = {
    Classic = {
        base = "Interface\\AddOns\\EmeraldClock\\Clock_Base.tga",
        hours = "Interface\\AddOns\\EmeraldClock\\Clock_Hours.tga",
        minutes = "Interface\\AddOns\\EmeraldClock\\Clock_Minutes.tga",
        seconds = "Interface\\AddOns\\EmeraldClock\\Clock_Seconds.tga",
        top = "Interface\\AddOns\\EmeraldClock\\Clock_Top.tga",
    }, Square = {
        base = "Interface\\AddOns\\EmeraldClock\\Clock_Base_square.tga",
        hours = "Interface\\AddOns\\EmeraldClock\\Clock_Hours.tga",
        minutes = "Interface\\AddOns\\EmeraldClock\\Clock_Minutes.tga",
        seconds = "Interface\\AddOns\\EmeraldClock\\Clock_Seconds.tga",
        top = "Interface\\AddOns\\EmeraldClock\\Clock_Top.tga",
    }
}
local options
function showOptions()
    if not options then
        options = EFrame.Window()
        options.title = "EmeraldClock Configuration"
        options.centralItem = EFrame.ColumnLayout(options)
        options.defaultStyle = EFrame.ComboBox(options.centralItem)
        local styles = {}
        local i, current = 1, nil
        for k, v in pairs(skins) do
            tinsert(styles, k)
            if i then
                if clock.skin == v then
                    current = i
                    i = nil
                else
                    i = i +1
                end
            end
        end
        options.defaultStyle.model = styles
        options.defaultStyle.currentIndex = current
        function options.defaultStyle:onActivated(what)
            EmeraldClockSettings.skin = styles[what]
            clock.skin = skins[styles[what]]
        end
        
        options.scaleLabel = EFrame.Label(options.centralItem)
        options.scaleLabel.text = EFrame.bind(function() return format("Scale: %.2f", clock.baseScale) end)
        
        options.scaleSlider = EFrame.Slider(options.centralItem)
        options.scaleSlider.from = 0.5
        options.scaleSlider.to = 2
        options.scaleSlider.value = EFrame.bind(clock, "baseScale")
        options.scaleSlider:connect("moved", function() clock.baseScale = options.scaleSlider.__value end)
        
        options.fontSize = EFrame.Label(options.centralItem)
        options.fontSize.text = EFrame.bind(function() return format("Font Size: %d", clock.fontSize) end)
        
        options.fontSlider = EFrame.Slider(options.centralItem)
        options.fontSlider.from = 6
        options.fontSlider.to = 42
        options.fontSlider.step = 1
        options.fontSlider.value = EFrame.bind(clock, "fontSize")
        options.fontSlider:connect("moved", function() clock.fontSize = options.fontSlider.__value end)
        
        options.color = EFrame.Button(options.centralItem)
        options.color.text = "Color"
        local function ShowColorPicker(r, g, b, a, changedCallback)
            ColorPickerFrame:SetColorRGB(r,g,b);
            ColorPickerFrame.hasOpacity, ColorPickerFrame.opacity = (a ~= nil), 1 - a or 0;
            ColorPickerFrame.previousValues = {r,g,b,a};
            ColorPickerFrame.func, ColorPickerFrame.opacityFunc, ColorPickerFrame.cancelFunc = changedCallback, changedCallback, changedCallback;
            ColorPickerFrame:Hide();
            ColorPickerFrame:Show();
        end
        function options.color.onClicked()
            local color = clock.__color
            ShowColorPicker(color[1], color[2], color[3], color[4], function (restore)
                if restore then
                    clock.color = restore
                else
                    local r, g, b = ColorPickerFrame:GetColorRGB()
                    clock.color = {r, g, b, 1 - OpacitySliderFrame:GetValue()}
                end
            end)
        end
        options.color.z = 3
    else
        options.visible = not options.visible
    end
end

function initUI()
    clock = EFrame.Window()
    EmeraldClock.clock = clock
    clock:attach("skin")
    clock:attach("baseScale")
    clock:attach("fontSize")
    clock.__baseScale = EFrame.normalize(1)
    clock.__skin = skins.Classic
    _, clock.__fontSize = GameFontNormal:GetFont()

    clock:attach("color")
    clock.__color = {1,1,1,1}
    function clock:onColorChanged(v)
        EmeraldClockSettings.color = v
    end

    clock.centralItem = EFrame.ColumnLayout(clock)

    local datel = EFrame.Label(clock.centralItem)
    datel.Layout.alignment = EFrame.Layout.AlignHCenter
    datel.fontSize = EFrame.bind(clock, "fontSize")

    local now = time()
    local local_tz = (now - time(date("!*t", now)))/3600
    local nowd = date("!*t")
    local shour, smin = GetGameTime()
    local server_tz = (time(nowd) - time(date("!*t", now)))/60
    server_tz = floor(server_tz/15+0.5)*15
    if server_tz > 12 then
        server_tz = server_tz - 24
    end
    if server_tz < -12 then
        server_tz = server_tz + 24
    end

    local function timezone(t)
        return format("UTC%+03d:%02d", floor(t), (t-floor(t))*60)
    end

    local stz = EFrame.Label(clock.centralItem)
    stz.Layout.alignment = EFrame.Layout.AlignHCenter
    stz.text = "Timezone: " .. timezone(server_tz)
    stz.fontSize = EFrame.bind(clock, "fontSize")
    local ltz = EFrame.Label(clock.centralItem)
    ltz.Layout.alignment = EFrame.Layout.AlignHCenter
    ltz.text = "Timezone: " .. timezone(local_tz)
    ltz.fontSize = EFrame.bind(clock, "fontSize")

    clock.base = EFrame.Image(clock.centralItem)
    clock.hours = EFrame.Image(clock.base)
    clock.minutes = EFrame.Image(clock.hours)
    clock.seconds = EFrame.Image(clock.minutes)
    clock.top = EFrame.Image(clock.seconds)

    clock.base.Layout.alignment = EFrame.Layout.AlignHCenter
    
    clock.base.implicitHeight = EFrame.bind(function() return 128 * clock.baseScale end)
    clock.base.implicitWidth = EFrame.bind(clock.base, "implicitHeight")

    local optsRow = EFrame.RowLayout(clock.centralItem)
    optsRow.Layout.fillWidth = true
    optsRow.spacing = 4
    optsRow.visible = EFrame.bind(function() return not clock.locked end)

    clock.localTime = EFrame.CheckButton(optsRow)
    clock.localTime.text = "Local time"
    clock.localTime:connect("checkedChanged", function(v) EmeraldClockSettings.localTime = v end)

    ltz.visible = EFrame.bind(function() return clock.localTime.checked end)
    stz.visible = EFrame.bind(function() return not clock.localTime.checked end)

    local optionsBtn = EFrame.Button(optsRow)
    optionsBtn.Layout.alignment = EFrame.Layout.AlignRight
    optionsBtn.text = "Options"
    optionsBtn.onClicked = showOptions

    clock.hours.anchorFill = clock.base
    clock.minutes.anchorFill = clock.hours
    clock.seconds.anchorFill = clock.minutes
    clock.top.anchorFill = clock.seconds

    for k in pairs(skins.Classic) do
        clock[k].source = EFrame.bind(function() return clock.skin[k] end)
    end

    clock.base.color = EFrame.bind(clock, "color")
    clock.hours.color = EFrame.bind(clock, "color")
    clock.minutes.color = EFrame.bind(clock, "color")
    clock.seconds.color = EFrame.bind(clock, "color")
    clock.top.color = EFrame.bind(clock, "color")
    --[[
    local function outElastic(t, b, c, d, a, p)
        assert(not a or a >= 1)
        a = (c-b)*(a or 1)
        p = 1/p
        local f = math.acos((c-b)/(a))
        return ((c-b)-math.exp(-t/d*6)*math.cos((t)*2*math.pi*p+f)*a)+b
    end]]


    clock:attach("sh")
    clock:attach("sm")
    clock:attach("ss")
    clock:attach("lh")
    clock:attach("lm")
    clock:attach("ls")
    clock:attach("h")
    clock:attach("m")
    clock:attach("s")
    clock.__sh = 0
    clock.__sm = 0
    clock.__ss = 0
    clock.__lh = 0
    clock.__lm = 0
    clock.__ls = 0

    clock.h = EFrame.bind(function() return clock.localTime.checked and clock.lh or clock.sh end)
    clock.m = EFrame.bind(function() return clock.localTime.checked and clock.lm or clock.sm end)
    clock.s = EFrame.bind(function() return clock.localTime.checked and clock.ls or clock.ss end)

    clock.seconds.anim = EFrame.PropertyAnimation(clock.seconds)
    clock.seconds.anim.property = "rotation"
    clock.seconds.anim.duration = 0.125
    clock.seconds.anim.easingCurve = EFrame.PropertyAnimation.OutElastic

    clock.hours.rotation = EFrame.bind(function() return (clock.h + (clock.m + clock.s/60)/60) /12 * 2 * math.pi end)
    clock.minutes.rotation = EFrame.bind(function() return (clock.m + clock.s/60) /60 * 2 * math.pi end)
    clock:connect("sChanged", function ()
        clock.seconds.anim.running = false
        clock.seconds.anim.from = (clock.s - 1) / 60 * 2 * math.pi
        clock.seconds.anim.to = clock.s / 60 * 2 * math.pi
        clock.seconds.anim.running = true
    end)
        
    local dateCheck
    local mins, secs
    local last = GetTime()
    EmeraldClock:SetScript("OnUpdate", function()
        local h, m = GetGameTime()
        if mins ~= m then
            secs = GetTime()
            mins = m
        end
        local s = 0
        if secs then
            s = GetTime() - secs
            s = math.floor(s)
        end
        clock.sh, clock.sm, clock.ss = h , m, s
        if GetTime() - last < .333 then return end
        last = GetTime()
        local t = date("*t")
        if not dateCheck or t.hour == 0 and dateCheck == 23 then
            datel.text = date("%a %d %B %Y")
            dateCheck = t.hour
        end
        
        clock.lh, clock.lm, clock.ls = t.hour, t.min, t.sec
    end)

    function clock:onMarginLeftChanged(x)
        EmeraldClockSettings.x = x
    end

    function clock:onMarginTopChanged(y)
        EmeraldClockSettings.y = y
    end

    clock.title = "EmeraldClock"

    clock.visible = true
    clock.background.visible = false
    
    clock.skin = skins[EmeraldClockSettings.skin or "Classic"]
    clock.marginLeft = EmeraldClockSettings.x
    clock.marginTop = EmeraldClockSettings.y
    clock.localTime.checked = EmeraldClockSettings.localTime
    if EmeraldClockSettings.color then
        clock.color = EmeraldClockSettings.color
    end
    clock.locked = EmeraldClockSettings.locked
    clock:connect("lockedChanged", function(v) EmeraldClockSettings.locked = v end)
    
    if EmeraldClockSettings.baseScale then
        clock.baseScale = EmeraldClockSettings.baseScale
    end
    clock:connect("baseScaleChanged", function(s) EmeraldClockSettings.baseScale = s end)
    
    if EmeraldClockSettings.fontSize then
        clock.fontSize = EmeraldClockSettings.fontSize
    end
    clock:connect("fontSizeChanged", function(s) EmeraldClockSettings.fontSize = s end)
end


EmeraldClock:SetScript("OnEvent", function(_this, _event, name)
    local name = name or arg1
    local event = _event or event
    local this = _this or this
    if event == "ADDON_LOADED" then
        if name ~= "EmeraldClock" then return end
        if not EmeraldClockSettings then
            EmeraldClockSettings = {x = 50, y = 50}
        end
        this:UnregisterEvent("ADDON_LOADED")
        initUI()
    end
end)

EmeraldClock:RegisterEvent("ADDON_LOADED")


local function chatcmd(msg)
    if not msg or msg == "" then
        showOptions()
        print("EClock: /eclock lock/unlock")
        return
    elseif msg == "show" then
        clock.visible = true
    elseif msg == "lock" then
        clock.locked = true
    elseif msg == "unlock" then
        clock.locked = false
    end
end

_G.SLASH_EMERALDCLOCK1 = "/emeraldclock"
_G.SLASH_EMERALDCLOCK2 = "/eclock"
_G.SlashCmdList["EMERALDCLOCK"] = chatcmd
